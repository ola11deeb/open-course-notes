# רשימות קורסים חופשיות

להלן קישורים מהירים לכל הרשימות כ-pdf-ים:
- 104122 - תורת הפונקציות | [הרצאות (רוס)](104122l-function-theory-aka-complex-analysis.pdf) | [תרגולים (רן)](104122r-function-theory-aka-complex-analysis.pdf)
- 104165 - פונקציות ממשיות | [הרצאות (לירן)](104165l-real-functions.pdf) | [תרגולים (אלון)](104165r-real-functions.pdf)
- 104279 - מבוא לחוגים ושדות | [הרצאות (מיכה)](104279l-introduction-to-rings-and-fields.pdf) | [תרגולים (סתיו)](104279r-introduction-to-rings-and-fields.pdf)
- 104295 - אינפי 3 | [הרצאות (לידיה)](104295l-infi-3.pdf) | [תרגולים (ליטל)](104295r-infi-3.pdf)

# תיקונים, הערות, הארות והצעות לשיפור
תמיד מוזמנים/ות לפתוח MR או ליצור קשר בכל אופן אחר :slight_smile:

# הגדרות LyX
הרשימות נערכות עם ההגדרות הנמצאות [כאן](https://gitlab.com/open-course-notes/lyx-config)

# מוטיבציה
- למה הרשימות פתוחות? [Learn in public](https://www.swyx.io/learn-in-public/)
- למה הרשימות חופשיות?
    - [What is free software? (GNU)](https://www.gnu.org/philosophy/free-sw.en.html)
    - [What is free software? (FSF)](https://www.fsf.org/about/what-is-free-software)
